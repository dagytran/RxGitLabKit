//
//  Enpoint.swift
//  RxGitLabKit
//
//  Created by Dagy Tran on 06/08/2018.
//

import Foundation
import RxSwift

/// This is a base class for all EndpointGroups
public class EndpointGroup {
    /// Communicator
    let hostCommunicator: HostCommunicator
    let disposeBag = DisposeBag()

    /// Endpoint enumeration
    enum Endpoints {}

    public required init(with hostCommunicator: HostCommunicator) {
        self.hostCommunicator = hostCommunicator
    }

    /// Object of type T from APIRequest
    ///
    /// - Parameter apiRequest: api request
    /// - Returns: Observable<T>
    func object<T>(for apiRequest: APIRequest) -> Observable<T> where T: Codable {
        hostCommunicator.object(for: apiRequest)
    }

    /// A server response with data from APIRequest
    ///
    /// - Parameter apiRequest: api request
    /// - Returns: Observable<(response: HTTPURLResponse, data: Data?)>
    func data(for apiRequest: APIRequest) -> Observable<Data> {
        hostCommunicator.data(for: apiRequest)
    }

    /// A server response with data from APIRequest
    ///
    /// - Parameter apiRequest: api request
    /// - Returns: Observable<(response: HTTPURLResponse, data: Data?)>
    func response(for apiRequest: APIRequest) -> Observable<(response: HTTPURLResponse, data: Data?)> {
        hostCommunicator.response(for: apiRequest)
    }

    /// A server response without data from APIRequest
    ///
    /// - Parameter apiRequest: api request
    /// - Returns: Observable<HTTPURLResponse>
    func httpURLResponse(for apiRequest: APIRequest) -> Observable<HTTPURLResponse> {
        hostCommunicator.httpURLResponse(for: apiRequest)
    }
}
