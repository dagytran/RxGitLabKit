//
//  Date+Extensions.swift
//  RxGitLabKit
//
//  Created by Dagy Tran on 21/10/2018.
//

import Foundation

extension Date {
    /// Returns a Date from String if successful
    ///
    /// - Parameters:
    ///   - string: Date
    ///   - formatter: A Date Formatter (default: ISO8601 Full)
    init?(from string: String, using formatter: DateFormatter = DateFormatter.iso8601full) {
        if let date = formatter.date(from: string) {
            self = date
        } else {
            return nil
        }
    }

    /// Returns a ISO8601 String representation
    var asISO8601String: String {
        DateFormatter.iso8601.string(from: self)
    }

    /// Returns a String representation in "yyyy-MM-dd" format
    var asyyyyMMddString: String {
        DateFormatter.yyyyMMdd.string(from: self)
    }
}
