//
//  HostCommunicator.swift
//  RxGitLabKit
//
//  Created by Dagy Tran on 31/10/2018.
//

import Foundation
import RxRelay
import RxSwift

/// Facilitates the communication between the client and server
///
/// For authenticated communication, it uses a Private or OAuth token
public class HostCommunicator {
    /// Networking
    private let network: Networking
    private let disposeBag = DisposeBag()

    /// Returns an authorization header based on provided token
    private var authorizationHeader: Header {
        var header = Header()
        if let privateToken = privateToken {
            header["Private-Token"] = privateToken
        }
        if let oAuthToken = oAuthTokenVariable.value {
            header["Authorization"] = "Bearer \(oAuthToken)"
        }
        return header
    }

    /// The URL of the GitLab Host
    public var hostURL: URL

    /// Private token used for authorized communication
    public var privateToken: String?

    /// OAuth token used for authorized communication
    public let oAuthTokenVariable = BehaviorRelay<String?>(value: nil)

    public init(network: Networking, hostURL: URL) {
        self.network = network
        self.hostURL = hostURL
    }

    public convenience init(hostURL: URL) {
        self.init(network: HTTPClient(using: URLSession.shared), hostURL: hostURL)
    }

    /// Header from APIRequest
    ///
    /// - Parameter apiRequest: api request
    /// - Returns: Observable<Header>
    public func header(for apiRequest: APIRequest) -> Observable<Header> {
        apiRequest.buildRequest(with: hostURL, header: authorizationHeader)
            .map(network.header) ?? Observable.error(HTTPError.invalidRequest(message: "Request building failed."))
    }

    /// Object of type T from APIRequest
    ///
    /// - Parameter apiRequest: api request
    /// - Returns: Observable<T>
    public func object<T>(for apiRequest: APIRequest) -> Observable<T> where T: Codable {
        apiRequest.buildRequest(with: hostURL, header: authorizationHeader)
            .map(network.object) ?? Observable.error(HTTPError.invalidRequest(message: "Request building failed."))
    }

    /// Data from APIRequest
    ///
    /// - Parameter apiRequest: api request
    /// - Returns: Observable<Data>
    public func data(for apiRequest: APIRequest) -> Observable<Data> {
        apiRequest.buildRequest(with: hostURL, header: authorizationHeader)
            .map(network.data) ?? Observable.error(HTTPError.invalidRequest(message: "Request building failed."))
    }

    /// A server response with data from APIRequest
    ///
    /// - Parameter apiRequest: api request
    /// - Returns: Observable<(response: HTTPURLResponse, data: Data?)>
    public func response(for apiRequest: APIRequest) -> Observable<(response: HTTPURLResponse, data: Data?)> {
        apiRequest.buildRequest(with: hostURL, header: authorizationHeader)
            .map(network.response) ?? .error(HTTPError.invalidRequest(message: "Request building failed."))
    }

    /// A server response without data from APIRequest
    ///
    /// - Parameter apiRequest: api request
    /// - Returns: Observable<HTTPURLResponse>
    public func httpURLResponse(for apiRequest: APIRequest) -> Observable<HTTPURLResponse> {
        response(for: apiRequest).map(\.response)
    }
}
