//
//  AppDelegate.swift
//  RxGitLabKitDemoApp
//
//  Created by Dagy Tran on 17/11/2018.
//  Copyright © 2018 Dagy Tran. All rights reserved.
//

import RxGitLabKit
import RxSwift
import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, UISplitViewControllerDelegate {
    var window: UIWindow?
    private let disposeBag = DisposeBag()

    func application(_: UIApplication, didFinishLaunchingWithOptions _: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        let gitlabClient = RxGitLabAPIClient(with: URL(string: "https://gitlab.com")!)

        // Setting up Projects tab in split view
        let projectsVC = ProjectsViewController()
        projectsVC.viewModel = ProjectsViewModel(with: gitlabClient)
        let detailVC = CommitDetailViewController()
        let splitVC = BaseSplitViewController()
        splitVC.viewControllers = [
            UINavigationController(rootViewController: projectsVC),
            UINavigationController(rootViewController: detailVC)
        ]
        let folderImage = UIImage(systemName: "folder.fill")!
        splitVC.tabBarItem = UITabBarItem(title: L10N.Main.projects, image: folderImage, selectedImage: folderImage)

        // Setting up profile tab
        let loginVC = LoginViewController()
        loginVC.viewModel = LoginViewModel(using: gitlabClient)
        let settingsNavigationController = UINavigationController(rootViewController: loginVC)
        let userImage = UIImage(systemName: "person.fill")!
        settingsNavigationController.tabBarItem = UITabBarItem(title: L10N.Main.user, image: userImage, selectedImage: userImage)

        // Tab Controller
        let tabController = UITabBarController()
        tabController.setViewControllers([splitVC, settingsNavigationController], animated: true)

        // Showing the screens
        window = UIWindow(frame: UIScreen.main.bounds)
        window?.rootViewController = tabController
        window?.makeKeyAndVisible()

        return true
    }
}
