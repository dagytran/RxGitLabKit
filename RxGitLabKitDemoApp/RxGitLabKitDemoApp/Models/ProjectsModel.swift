//
//  ProjectsModel.swift
//  RxGitLabKitDemoApp
//
//  Created by Dagy Tran on 25/12/2018.
//  Copyright © 2018 Dagy Tran. All rights reserved.
//

import Foundation
import RxGitLabKit
import RxRelay
import RxSwift

class ProjectsModel {
    let projects = BehaviorRelay<[Project]>(value: [])
    let userProjects = BehaviorRelay<[Project]>(value: [])
}
