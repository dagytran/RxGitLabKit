//
//  L10N.swift
//  RxGitLabKitDemoApp
//
//  Created by Dagy Tran on 21.10.2021.
//  Copyright © 2021 Dagy Tran. All rights reserved.
//

import Foundation

/// A struct with all localized strings
struct L10N {

    struct Common {
        static let ok = "ok".localized
    }

    struct Project {
        static func detailText(starCount: Int, forkCount: Int) -> String {
            String.localizedStringWithFormat("project.detail".localized, starCount, forkCount)
        }
    }

    struct CommitDetail {
        static let title = "commitdetail.title".localized
    }

    struct Commits {
        static let commits = "commits.commits".localized
        static let emptyMessage = "commits.emptyMessage".localized
    }

    struct Commit {
        static let id = "commit.detail.id".localized
        static let shortID = "commit.detail.shortID".localized
        static let title = "commit.detail.title".localized
        static let authorName = "commit.detail.authorName".localized
        static let authorEmail = "commit.detail.authorEmail".localized
        static let authoredDate = "commit.detail.authoredDate".localized
        static let committerName = "commit.detail.committerName".localized
        static let committerEmail = "commit.detail.committerEmail".localized
        static let committedDate = "commit.detail.committedDate".localized
        static let createdAt = "commit.detail.createdAt".localized
        static let message = "commit.detail.message".localized
        static let parentIds = "commit.detail.parentIds".localized
        static let total = "commit.detail.total".localized
        static let additions = "commit.detail.additions".localized
        static let deletions = "commit.detail.deletions".localized
        static let status = "commit.detail.status".localized
    }

    struct Login {
        static let title = "login.title".localized
        static let authorize = "login.authorize".localized
        static let host = "login.host".localized
        static let credentials = "login.credentials".localized
        static let password = "login.password".localized
        static let privateToken = "login.privateToken".localized
        static let authToken = "login.authToken".localized
        static let loginFailed = "login.loginFailed".localized
        static let logout = "login.logout".localized
    }

    struct Profile {
        static let title = "profile.title".localized
    }

    struct Projects {
        static let title = "projects.title".localized
        static let searchProjects = "projects.search.projects".localized
        static let searchAllProjects = "projects.search.allProjects".localized
        static let searchUserProjects = "projects.search.userProjects".localized
        static let emptyMessage = "projects.emptyMessage".localized
    }

    struct Main {
        static let projects = "main.projects".localized
        static let user = "main.user".localized
    }

    static let user = ""
}

private extension String {
    var localized: String {
        NSLocalizedString(self, comment: "")
    }
}
