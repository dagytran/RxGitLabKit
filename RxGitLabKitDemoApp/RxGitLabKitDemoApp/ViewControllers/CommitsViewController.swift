//
//  CommitsViewController.swift
//  RxGitLabKitDemoApp
//
//  Created by Dagy Tran on 17/11/2018.
//  Copyright © 2018 Dagy Tran. All rights reserved.
//

import RxGitLabKit
import RxSwift
import UIKit

/// Shows a list of commits for a given project
class CommitsViewController: BaseViewController {
    // MARK: UI components

    private let tableView: UITableView = {
        let tableView = UITableView()
        tableView.register(CommitTableViewCell.self, forCellReuseIdentifier: CommitTableViewCell.cellIdentifier)

        return tableView
    }()

    private let loadingIndicator = UIActivityIndicatorView(style: .medium)

    // MARK: View Model

    var viewModel: CommitsViewModel!

    // MARK: Life cycle

    override func viewDidLoad() {
        super.viewDidLoad()
        title = L10N.Commits.commits
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        viewModel.loadNextProjectPage()
    }

    override func addUIComponents() {
        view.addSubview(tableView)
        tableView.tableFooterView = loadingIndicator
    }

    override func layoutUIComponents() {
        tableView.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }
    }

    override func setupBindings() {
        // Table data
        viewModel.dataSource
            .bind(
                to: tableView.rx.items(
                    cellIdentifier: CommitTableViewCell.cellIdentifier,
                    cellType: CommitTableViewCell.self
                )
            ) { _, element, cell in
                cell.commit = element
            }
            .disposed(by: disposeBag)

        // Show empty table message if the projects
        viewModel.dataSource
            .map { $0.isEmpty }
            .observe(on: MainScheduler.instance)
            .subscribe(onNext: { isEmpty in
                if isEmpty {
                    self.tableView.setEmptyMessage(L10N.Commits.emptyMessage)
                } else {
                    self.tableView.restore()
                }
            })
            .disposed(by: disposeBag)

        // Shows commit detail when tapped on item
        tableView.rx.itemSelected
            .subscribe(onNext: { indexPath in
                self.showDetail(indexPath)
            })
            .disposed(by: disposeBag)

        // Loads more objects when the table reaches the end
        tableView.rx.willBeginDecelerating
            .subscribe(onNext: { _ in
                if self.tableView.isReachingEnd {
                    self.viewModel.loadNextProjectPage()
                }
            })
            .disposed(by: disposeBag)

        // Loading indicator
        viewModel.isLoading
            .observe(on: MainScheduler.instance)
            .bind(to: loadingIndicator.rx.isAnimating)
            .disposed(by: disposeBag)
    }

    /// Shows commit detail
    private func showDetail(_ indexPath: IndexPath) {
        let commitDetailVC = CommitDetailViewController()
        commitDetailVC.viewModel = CommitDetailViewModel(
            with: viewModel.gitlabClient,
            commit: viewModel.commit(for: indexPath.row),
            projectID: viewModel.projectID
        )
        showDetailViewController(UINavigationController(rootViewController: commitDetailVC), sender: self)
    }
}
