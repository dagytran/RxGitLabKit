//
//  ProjectsViewController.swift
//  RxGitLabKitDemoApp
//
//  Created by Dagy Tran on 17/11/2018.
//  Copyright © 2018 Dagy Tran. All rights reserved.
//

import RxGitLabKit
import RxSwift
import UIKit

/// This view controller shows a list of projects.
///
/// It can show all projects or user projects
/// Using search can help filter the desired projects
class ProjectsViewController: BaseViewController {
    // MARK: UI Components

    private let searchBar: UISearchBar = {
        let searchBar = UISearchBar()
        searchBar.scopeButtonTitles = [L10N.Projects.searchAllProjects, L10N.Projects.searchUserProjects]
        searchBar.showsScopeBar = true
        searchBar.placeholder = L10N.Projects.searchUserProjects
        searchBar.sizeToFit()
        searchBar.scopeBarBackgroundImage = UIImage()
        searchBar.backgroundImage = UIImage()
        searchBar.returnKeyType = .done
        return searchBar
    }()

    private let tableView: UITableView = {
        let tableView = UITableView()
        tableView.register(ProjectTableViewCell.self, forCellReuseIdentifier: ProjectTableViewCell.cellIdentifier)
        return tableView
    }()

    private let loadingIndicator = UIActivityIndicatorView(style: .medium)

    // MARK: View Model

    var viewModel: ProjectsViewModel!

    // MARK: Life cycle

    override func viewDidLoad() {
        super.viewDidLoad()
        title = L10N.Profile.title
        view.backgroundColor = .white
    }

    // MARK: Setup

    // Adds the UI components to `view`
    override func addUIComponents() {
        view.addSubview(searchBar)
        view.addSubview(tableView)
        tableView.tableFooterView = loadingIndicator
    }

    // Applies constraints on UI Components
    override func layoutUIComponents() {
        searchBar.snp.makeConstraints { make in
            make.top.equalTo(view.safeAreaLayoutGuide.snp.top)
            make.leading.trailing.equalToSuperview()
        }

        tableView.snp.makeConstraints { make in
            make.leading.bottom.trailing.equalToSuperview()
            make.top.equalTo(searchBar.snp.bottom)
        }
    }

    override func setupBindings() {
        setupSearchBarBindings()
        setupTableViewBindings()
    }

    // MARK: Private functions

    private func setupSearchBarBindings() {
        // Scope button
        searchBar.rx
            .selectedScopeButtonIndex
            .map { $0 == 1 }
            .bind(to: viewModel.isUserTabSelectedVariable)
            .disposed(by: disposeBag)

        // Dismiss when done on keyboard is pressed
        searchBar.rx.searchButtonClicked
            .subscribe { _ in self.searchBar.endEditing(true) }
            .disposed(by: disposeBag)

        // Searchtext binding
        searchBar.rx.text
            .distinctUntilChanged()
            .bind(to: viewModel.searchTextVariable)
            .disposed(by: disposeBag)
    }

    private func setupTableViewBindings() {
        // Selecting item
        tableView.rx.itemSelected
            .subscribe(onNext: { [unowned self] indexPath in
                let commitsVC = CommitsViewController()
                commitsVC.viewModel = CommitsViewModel(
                    with: self.viewModel.gitlabClient,
                    projectID: self.viewModel.projectID(for: indexPath.row).id
                )
                self.navigationController?.pushViewController(commitsVC, animated: true)
            })
            .disposed(by: disposeBag)

        // Cell data
        viewModel.dataSource
            .bind(
                to: tableView.rx.items(
                    cellIdentifier: ProjectTableViewCell.cellIdentifier,
                    cellType: ProjectTableViewCell.self
                )
            ) { _, element, cell in
                cell.project = element
            }
            .disposed(by: disposeBag)

        // Empty label
        viewModel.dataSource
            .map(\.isEmpty)
            .observe(on: MainScheduler.instance)
            .subscribe(onNext: { isEmpty in
                if isEmpty {
                    self.tableView.setEmptyMessage(L10N.Projects.emptyMessage)
                } else {
                    self.tableView.restore()
                }
            })
            .disposed(by: disposeBag)

        // Load more projects
        tableView.rx
            .willBeginDecelerating
            .subscribe(onNext: { _ in
                if self.tableView.isReachingEnd {
                    if self.viewModel.isUserTabSelectedVariable.value {
                        self.viewModel.loadNextUserProjectPage()
                    } else {
                        self.viewModel.loadNextProjectPage()
                    }
                }
            })
            .disposed(by: disposeBag)

        // Loading indicator
        viewModel.isLoading
            .observe(on: MainScheduler.instance)
            .bind(to: loadingIndicator.rx.isAnimating)
            .disposed(by: disposeBag)
    }
}
