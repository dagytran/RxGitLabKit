//
//  CommitDetailViewModel.swift
//  RxGitLabKitDemoApp
//
//  Created by Dagy Tran on 18/11/2018.
//  Copyright © 2018 Dagy Tran. All rights reserved.
//

import Foundation
import RxGitLabKit
import RxRelay
import RxSwift

class CommitDetailViewModel: BaseViewModel {
    private let commitVariable: BehaviorRelay<Commit>

    /// Data for the table
    var dataSource: Observable<[(String, String)]> {
        commitVariable.asObservable()
            .map { commit -> [(String, String)] in
                var texts = [(String, String)]()
                texts.append((L10N.Commit.id, commit.id))
                texts.append((L10N.Commit.shortID, commit.shortId))

                if let title = commit.title {
                    texts.append((L10N.Commit.title, title))
                }
                texts.append((L10N.Commit.authorName, commit.authorName))

                if let authorEmail = commit.authorEmail {
                    texts.append((L10N.Commit.authorEmail, authorEmail))
                }
                if let authoredDate = commit.authoredDate?.localizedString {
                    texts.append((L10N.Commit.authoredDate, authoredDate))
                }

                texts.append((L10N.Commit.committerName, commit.committerName))

                texts.append((L10N.Commit.committerEmail, commit.committerEmail))

                if let committedDate = commit.committedDate?.localizedString {
                    texts.append((L10N.Commit.committedDate, committedDate))
                }
                if let createdAt = commit.createdAt?.localizedString {
                    texts.append((L10N.Commit.createdAt, createdAt))
                }
                if let message = commit.message {
                    texts.append((L10N.Commit.message, message))
                }
                if let parentIds = commit.parentIds?.joined(separator: ",") {
                    texts.append((L10N.Commit.parentIds, parentIds))
                }
                if let stats = commit.stats {
                    if let total = stats.total {
                        texts.append((L10N.Commit.total, String(total)))
                    }

                    if let additions = stats.additions {
                        texts.append((L10N.Commit.additions, String(additions)))
                    }

                    if let deletions = stats.deletions {
                        texts.append((L10N.Commit.deletions, String(deletions)))
                    }
                }

                if let status = commit.status {
                    texts.append((L10N.Commit.status, status))
                }
                return texts
            }
    }

    init(with gitlabClient: RxGitLabAPIClient, commit: Commit, projectID: Int) {
        commitVariable = BehaviorRelay<Commit>(value: commit)
        super.init()
        // load additional commit data
        gitlabClient.commits
            .getCommit(projectID: projectID, sha: commit.id)
            .bind(to: commitVariable)
            .disposed(by: disposeBag)
    }
}
