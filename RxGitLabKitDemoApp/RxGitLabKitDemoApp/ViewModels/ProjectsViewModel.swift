//
//  ProjectsViewModel.swift
//  RxGitLabKitDemoApp
//
//  Created by Dagy Tran on 18/11/2018.
//  Copyright © 2018 Dagy Tran. All rights reserved.
//

import Foundation
import RxCocoa
import RxGitLabKit
import RxRelay
import RxSwift

/// Projects model handles all projects and user projects
class ProjectsViewModel: BaseViewModel {
    // MARK: Private properties

    private let projects = BehaviorRelay<[Project]>(value: [])
    private let model = ProjectsModel()
    private let projectsEndpointGroup: ProjectsEnpointGroup!

    // Load next Triggers
    private let loadNextProjectPageTrigger = PublishSubject<Void>()
    private let loadNextUserProjectPageTrigger = PublishSubject<Void>()

    /// Publishes loading state changes
    private let isLoadingPublisher = PublishSubject<Bool>()

    /// Notifies if projects are loading
    var isLoading: Observable<Bool> {
        isLoadingPublisher.asObservable()
    }

    /// Projects pagination
    private let projectPaginator: BehaviorRelay<Paginator<Project>>
    private var lastProjectPage = 0

    /// User projects pagination
    private let userProjectPaginator = BehaviorRelay<Paginator<Project>?>(value: nil)
    private var lastUserProjectPage = 0

    /// Logged in user
    private let currentUserVariable = BehaviorRelay<User?>(value: nil)

    // MARK: Public properties

    let gitlabClient: RxGitLabAPIClient
    let isUserTabSelectedVariable = BehaviorRelay<Bool>(value: false)
    let searchTextVariable = BehaviorRelay<String?>(value: "")

    // MARK: Outputs

    // Publishing Projects out
    private let projectsPublisher = PublishSubject<[Project]>()
    var dataSource: Observable<[Project]> {
        projectsPublisher.asObservable()
    }

    init(with gitlabClient: RxGitLabAPIClient, driver _: Driver<Void>? = nil) {
        self.gitlabClient = gitlabClient
        projectsEndpointGroup = gitlabClient.projects

        projectPaginator = BehaviorRelay<Paginator<Project>>(value: projectsEndpointGroup.getProjects(parameters: [
            "simple": true,
            "sort": "asc"
        ] as [String: Any]))
        super.init()
        setupBindings()
    }

    private func setupBindings() {
        // Logged in User binding
        gitlabClient
            .currentUserObservable
            .bind(to: currentUserVariable)
            .disposed(by: disposeBag)

        // User Projects Paginator
        currentUserVariable.asObservable()
            .subscribe(onNext: { user in
                guard let user = user else {
                    self.userProjectPaginator.accept(nil)
                    return
                }
                self.userProjectPaginator.accept(self.projectsEndpointGroup.getUserProjects(userID: user.id, parameters: [
                    "simple": true,
                    "sort": "asc"
                ] as [String: Any]))
            })
            .disposed(by: disposeBag)

        // Text search
        let searchTextObservable = searchTextVariable.asObservable()
            .throttle(RxTimeInterval.milliseconds(200), scheduler: MainScheduler.instance)
            .share()

        // Creating Project Paginator and User paginator from search text
        searchTextObservable.subscribe(onNext: { searchText in
            let parameters = [
                "search": searchText ?? "",
                "simple": true,
                "sort": "asc"
            ] as [String: Any]
            self.model.userProjects.accept([])
            self.model.projects.accept([])
            if let user = self.currentUserVariable.value {
                self.userProjectPaginator.accept(self.projectsEndpointGroup.getUserProjects(userID: user.id, parameters: parameters))
            }

            self.projectPaginator.accept(self.projectsEndpointGroup.getProjects(parameters: parameters))
        })
        .disposed(by: disposeBag)

        // Load projects when a new paginator is set
        projectPaginator.asObservable()
            .do(onNext: { _ in
                self.lastProjectPage = 1
                self.isLoadingPublisher.onNext(true)
            })
            .flatMap { paginator -> Observable<[Project]> in
                paginator[self.lastProjectPage]
            }
            .bind(to: model.projects)
            .disposed(by: disposeBag)

        // Load user projects when a new paginator is set
        userProjectPaginator.asObservable()
            .do(onNext: { _ in
                self.lastUserProjectPage = 1
            })
            .flatMap { paginator -> Observable<[Project]> in
                guard let paginator = paginator else { return Observable.just([]) }
                return paginator[self.lastProjectPage].debug()
            }
            .bind(to: model.projects)
            .disposed(by: disposeBag)

        // Load next project page using the latest paginator
        loadNextProjectPageTrigger
            .map { self.projectPaginator.value }
            .flatMap { paginator -> Observable<[Project]> in
                paginator[self.lastProjectPage]
            }
            .map { self.model.projects.value + $0 }
            .subscribe(onNext: model.projects.accept)
            .disposed(by: disposeBag)

        // Load next user project page using the latest paginator
        loadNextUserProjectPageTrigger
            .map { self.userProjectPaginator.value }
            .filter { $0 != nil }
            .flatMap { paginator -> Observable<[Project]> in
                guard let paginator = paginator else { return Observable.just([]) }
                return paginator[self.lastUserProjectPage]
            }
            .map { self.model.projects.value + $0 }
            .bind(to: model.projects)
            .disposed(by: disposeBag)

        // Changes the data source when a different tab is selected
        isUserTabSelectedVariable.asObservable()
            .subscribe(onNext: { isUserTab in
                if isUserTab {
                    self.projectsPublisher.onNext(self.model.userProjects.value)
                } else {
                    self.projectsPublisher.onNext(self.model.projects.value)
                }
            })
            .disposed(by: disposeBag)

        // Publish that the projects are loading
        Observable.of(
            loadNextUserProjectPageTrigger.asObservable().debug(),
            loadNextProjectPageTrigger.asObservable().debug(),
            searchTextObservable.map { _ in () }.skip(1).debug()
        )
        .merge()
        .subscribe(on: MainScheduler.instance)
        .subscribe(onNext: { _ in
            self.isLoadingPublisher.onNext(true)
        })
        .disposed(by: disposeBag)

        // Publish that the loading has stopped
        Observable.combineLatest(
            model.projects.asObservable(),
            model.userProjects.asObservable(),
            isUserTabSelectedVariable.asObservable()
        )
        .subscribe(onNext: { projects, userProjects, isUser in
            self.projectsPublisher.onNext(isUser ? userProjects : projects)
            self.isLoadingPublisher.onNext(false)
        })
        .disposed(by: disposeBag)
    }

    // Returns a projectID
    func projectID(for index: Int) -> Project {
        isUserTabSelectedVariable.value ? model.userProjects.value[index] : model.projects.value[index]
    }

    // Load next projects page trigger
    func loadNextProjectPage() {
        lastProjectPage += 1
        loadNextProjectPageTrigger.onNext(())
    }

    // Load next user project page trigger
    func loadNextUserProjectPage() {
        if userProjectPaginator.value != nil {
            lastUserProjectPage += 1
            loadNextUserProjectPageTrigger.onNext(())
        }
    }
}
