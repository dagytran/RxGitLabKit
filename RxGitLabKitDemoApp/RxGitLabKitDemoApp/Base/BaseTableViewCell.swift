//
//  BaseTableViewCell.swift
//  RxGitLabKitDemoApp
//
//  Created by Dagy Tran on 18/11/2018.
//  Copyright © 2018 Dagy Tran. All rights reserved.
//

import RxSwift
import UIKit

/// A base table view cell with common attributes
class BaseTableViewCell: UITableViewCell {}
