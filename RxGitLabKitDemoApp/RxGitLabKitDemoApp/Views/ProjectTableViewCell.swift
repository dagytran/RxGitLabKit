//
//  ProjectTableViewCell.swift
//  RxGitLabKitDemoApp
//
//  Created by Dagy Tran on 18/11/2018.
//  Copyright © 2018 Dagy Tran. All rights reserved.
//

import RxGitLabKit
import RxSwift
import UIKit

class ProjectTableViewCell: BaseTableViewCell {
    var project: Project! {
        didSet {
            textLabel?.text = "\(project.namespace.name)/\(project.name)"
            detailTextLabel?.text = L10N.Project.detailText(starCount: project.starCount, forkCount: project.forksCount)
        }
    }

    override init(style _: UITableViewCell.CellStyle, reuseIdentifier _: String?) {
        super.init(style: .subtitle, reuseIdentifier: ProjectTableViewCell.cellIdentifier)
        textLabel?.textColor = UIColor.blue
    }

    @available(*, unavailable)
    required init?(coder _: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
