//
//  CommitTableViewCell.swift
//  RxGitLabKitDemoApp
//
//  Created by Dagy Tran on 18/11/2018.
//  Copyright © 2018 Dagy Tran. All rights reserved.
//

import RxGitLabKit
import RxSwift
import UIKit

class CommitTableViewCell: BaseTableViewCell {
    var commit: Commit! {
        didSet {
            textLabel?.text = commit.title
            detailTextLabel?.text = "\(commit.authorName) \t \(commit.shortId)\t \(commit.authoredDate?.localizedString ?? "")"
        }
    }

    override init(style _: UITableViewCell.CellStyle, reuseIdentifier _: String?) {
        super.init(style: .subtitle, reuseIdentifier: ProjectTableViewCell.cellIdentifier)
        textLabel?.textColor = UIColor.blue
    }

    @available(*, unavailable)
    required init?(coder _: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
