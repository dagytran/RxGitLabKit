//
//  CommitDetailTableViewCell.swift
//  RxGitLabKitDemoApp
//
//  Created by Dagy Tran on 18/11/2018.
//  Copyright © 2018 Dagy Tran. All rights reserved.
//

import UIKit

class CommitDetailTableViewCell: UITableViewCell {
    override init(style _: UITableViewCell.CellStyle, reuseIdentifier _: String?) {
        super.init(style: .value1, reuseIdentifier: CommitDetailTableViewCell.cellIdentifier)
        textLabel?.textColor = UIColor.blue
    }

    @available(*, unavailable)
    required init?(coder _: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
