//
//  BaseModelTestCase.swift
//  RxGitLabKit
//
//  Created by Dagy Tran on 04/01/2019.
//

import Foundation
@testable import RxGitLabKit
import XCTest

class BaseModelTestCase: XCTestCase {
    let decoder: JSONDecoder = {
        let decoder = JSONDecoder()
        decoder.dateDecodingStrategy = .formatted(DateFormatter.iso8601full)
        return decoder
    }()

    let calendar = Calendar(identifier: .gregorian)
}
