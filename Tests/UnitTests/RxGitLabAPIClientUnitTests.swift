//
//  RxGitLabAPIClientUnitTests.swift
//  RxGitLabKit-iOSTests
//
//  Created by Dagy Tran on 31/10/2018.
//

import Foundation
@testable import RxGitLabKit
import RxSwift
import XCTest

class RxGitLabAPIClientUnitTests: XCTestCase {
    private var client: RxGitLabAPIClient!
    private var mockSession: MockURLSession!

    private let hostURL = URL(string: "test.gitlab.com")!

    override func setUp() {
        super.setUp()
        mockSession = MockURLSession()
        let hostCommunicator = HostCommunicator(network: HTTPClient(using: mockSession), hostURL: hostURL)
        client = RxGitLabAPIClient(with: hostCommunicator)
    }

    func testLogin() {
        mockSession.nextData = AuthenticationMocks.oAuthResponseData
        client.logIn(username: AuthenticationMocks.username, password: AuthenticationMocks.password)
        let result = client.hostCommunicator.oAuthTokenVariable.asObservable()
            .take(1)
            .toBlocking()
            .materialize()

        switch result {
        case let .completed(elements: elements):
            XCTAssertNotNil(elements.first ?? nil)
            XCTAssertNil(client.privateToken)
        case .failed(elements: _, error: let error):
            XCTFail((error as? HTTPError)?.errorDescription ?? error.localizedDescription)
        }
    }
}
