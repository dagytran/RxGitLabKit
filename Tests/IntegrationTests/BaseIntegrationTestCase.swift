//
//  BaseIntegrationTests.swift
//  RxGitLabKit
//
//  Created by Dagy Tran on 27/11/2018.
//

import Foundation
@testable import RxGitLabKit
import RxSwift
import XCTest

class BaseIntegrationTestCase: XCTestCase {
    var client: RxGitLabAPIClient!
    var hostCommunicator: HostCommunicator!
    var disposeBag: DisposeBag!
    let hostURL = URL(string: "http://localhost:80")!
    let calendar = Calendar(identifier: .gregorian)

    override func setUp() {
        super.setUp()
        hostCommunicator = HostCommunicator(hostURL: hostURL)
        client = RxGitLabAPIClient(with: hostURL, privateToken: "pxu4zszRBBoEe9bsybGc")
        disposeBag = DisposeBag()
    }

    override func tearDown() {
        super.tearDown()
        disposeBag = nil
    }
}
