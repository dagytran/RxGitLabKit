//
//  RxGitLabAPIClientIntegrationTests.swift
//  RxGitLabKit-iOSTests
//
//  Created by Dagy Tran on 20/08/2018.
//

import Foundation
@testable import RxGitLabKit
import RxSwift
import XCTest

class RxGitLabAPIClientIntegrationTests: BaseIntegrationTestCase {
    func testLogin() {
        client.logOut()
        client.logIn(username: "root", password: "admin12345")
        let result = client.hostCommunicator.oAuthTokenVariable.asObservable()
            .take(1)
            .filter { $0 != nil }
            .toBlocking()
            .materialize()

        switch result {
        case let .completed(elements: elements):
            XCTAssertNotNil(elements.first ?? nil)
            XCTAssertNil(client.privateToken)
        case .failed(elements: _, error: let error):
            XCTFail((error as? HTTPError)?.errorDescription ?? error.localizedDescription)
        }
    }
}
