// swift-tools-version:5.2
// The swift-tools-version declares the minimum version of Swift required to build this package.

import PackageDescription

let package = Package(
    name: "RxGitLabKit",
    platforms: [
        .macOS(.v10_15),
        .iOS(.v11),
        .tvOS(.v11),
        .watchOS(.v2)
    ],
    products: [
        .library(
            name: "RxGitLabKit",
            targets: ["RxGitLabKit"]
        )
    ],
    dependencies: [
        .package(url: "https://github.com/ReactiveX/RxSwift.git", from: "6.2.0")
    ],
    targets: [
        .target(
            name: "RxGitLabKit",
            dependencies: [
                .product(name: "RxCocoa", package: "RxSwift"),
                "RxSwift"
            ],
            path: "Sources"
        ),

        .testTarget(
            name: "UnitTests",
            dependencies: [
                "RxGitLabKit",
                "RxSwift",
                .product(name: "RxBlocking", package: "RxSwift"),
                .product(name: "RxTest", package: "RxSwift")
            ],
            path: "Tests",
            exclude: ["IntegrationTests"]
        )
    ]
)
